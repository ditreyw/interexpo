this["JST"] = this["JST"] || {};

this["JST"]["assets/templates/test.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += 'test';

}
return __p
};

this["JST"]["assets/js/admin/templates/adminContentPanel.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<h2 class="col-sm-12">' +
((__t = ( name.toUpperCase() )) == null ? '' : __t) +
'</h2>\n\n<div class="col-sm-12 controller controller--content-panel">\n\t<!-- <div class="controller--content-panel__item">\n\t\t<p>Add new</p>\n\t</div>\n\t<div class="controller--content-panel__item">\n\t\t<p>Refresh</p>\n\t</div> -->\n\t<button class="controller--content-panel__item controller--content-panel__item--create"><i class="glyphicon glyphicon-plus"></i></button>\n\t<button class="controller--content-panel__item controller--content-panel__item--refresh"><i class="glyphicon glyphicon-refresh"></i></button>\n</div>\n\n<div class="col-sm-24 content-panel__form-wr"></div>';

}
return __p
};

this["JST"]["assets/js/admin/templates/adminMenuItem.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<a href="#' +
((__t = ( name )) == null ? '' : __t) +
'">\n';
 if (type == 'list') { ;
__p += '\n\t<i class="adminMenu__item__icon glyphicon glyphicon-align-justify"></i>\n';
 } ;
__p += '\n<h4>' +
((__t = ( name.charAt(0).toUpperCase() + name.slice(1) )) == null ? '' : __t) +
'</h4>\n</a>';

}
return __p
};

this["JST"]["assets/js/admin/templates/contentModelView.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '\t<div class="content__item__inner">\n\t\t<div class="content__item__header">\n\t\t\t<h3 class="content__item__titel">' +
((__t = ( name )) == null ? '' : __t) +
'</h3>\n\t\t\t<div class="controller controller--content__item">\n\t\t\t\t<div class="controller__item controller__item--collapse">\n\t\t\t\t\t<i class="glyphicon glyphicon-chevron-up"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class="controller__item controller__item--expande">\n\t\t\t\t\t<i class="glyphicon glyphicon-chevron-down"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class="controller__item controller__item--edit">\n\t\t\t\t\t<i class="glyphicon glyphicon-pencil"></i>\n\t\t\t\t</div>\n\t\t\t\t<div class="controller__item controller__item--delete">\n\t\t\t\t\t<i class="glyphicon glyphicon-remove"></i>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="content__item__text">\n\t\t\t';
 for (var characteristic in characteristics) { ;
__p += '\n\t\t\t\t';
 if (characteristics[characteristic].view_type === 'text' || characteristics[characteristic].view_type === 'varchar' || characteristics[characteristic].view_type === 'integer') { ;
__p += '\n\t\t\t\t\t<div class="content__item__text__item">\n\t\t\t\t\t\t<div class="content__item__text__item__titel">\n\t\t\t\t\t\t\t<h4>' +
((__t = ( characteristics[characteristic].view_name.toUpperCase().replace(/_/g, " ") + " : " )) == null ? '' : __t) +
'</h4>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="content__item__text__item__text">\n\t\t\t\t\t\t\t<p> ' +
((__t = ( characteristics[characteristic].innerText )) == null ? '' : __t) +
'</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="clearfix"></div>\n\t\t\t\t\t</div>\n\t\t\t\t';
 } else if (characteristics[characteristic].view_type === 'img') { ;
__p += '\n\t\t\t\t\t<div class="content__item__text__item">\n\t\t\t\t\t\t<div class="content__item__text__item__titel">\n\t\t\t\t\t\t\t<h4>' +
((__t = ( characteristics[characteristic].view_name.toUpperCase().replace(/_/g, " ") + " : " )) == null ? '' : __t) +
'</h4>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="content__item__text__item__text">\n\t\t\t\t\t\t\t';
 if (characteristics[characteristic].innerText) { ;
__p += ' \n\t\t\t\t\t\t\t\t';
 for (var i=0; i<characteristics[characteristic].innerText.length; i++) { ;
__p += '\n\t\t\t\t\t\t\t\t\t<p><img style="width:20%;float:left;margin-right:15px;"src="images/Projects/' +
((__t = ( characteristics[characteristic].innerText[i] )) == null ? '' : __t) +
'"></p>\n\t\t\t\t\t\t\t\t';
 } ;
__p += '\n\t\t\t\t\t\t\t';
 } ;
__p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="clearfix"></div>\n\t\t\t\t\t</div>\n\t\t\t\t';
 } ;
__p += ' \n\t\t\t';
 } ;
__p += '\n\t\t</div>\n\t</div>\n\t<form enctype="multipart/form-data">\n\t\t<div class="content__item__header">\n\t\t\t<input type="text" value="' +
((__t = ( name )) == null ? '' : __t) +
'" name="name">\n\t\t</div>\n\t\t<div class="content__item__text">\n\t\t\t';
 for (var characteristic in characteristics) { ;
__p += '\n\t\t\t\t<!-- <div class="content__item__text__item">\n\t\t\t\t\t<div class="content__item__text__item__titel">\n\t\t\t\t\t\t<h4>' +
((__t = ( characteristics[characteristic].view_name.toUpperCase().replace(/_/g, " ") + " : " )) == null ? '' : __t) +
'</h4>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="content__item__text__item__text">\n\t\t\t\t\t\t<textarea data-type="application/json" ';
if (characteristics[characteristic].required){
							;
__p += ' required="required" ';

							};
__p += ' type="text" data-dbFieldName="' +
((__t = ( characteristic )) == null ? '' : __t) +
'">' +
((__t = ( characteristics[characteristic].innerText )) == null ? '' : __t) +
'</textarea>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="clearfix"></div>\n\t\t\t\t</div> -->\n\t\t\t\t';
 if (characteristics[characteristic].view_type === 'text' || characteristics[characteristic].view_type === 'varchar' || characteristics[characteristic].view_type === 'integer') { ;
__p += '\n\t\t\t\t\t<div class="content__item__text__item">\n\t\t\t\t\t\t<div class="content__item__text__item__titel">\n\t\t\t\t\t\t\t<h4>' +
((__t = ( characteristics[characteristic].view_name.toUpperCase().replace(/_/g, " ") + " : " )) == null ? '' : __t) +
'</h4>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="content__item__text__item__text">\n\t\t\t\t\t\t\t<textarea data-type="application/json" name="' +
((__t = ( characteristic )) == null ? '' : __t) +
'" ';
if (characteristics[characteristic].required){
							;
__p += ' required="required" ';

							};
__p += ' type="text" data-dbFieldName="' +
((__t = ( characteristic )) == null ? '' : __t) +
'">' +
((__t = ( characteristics[characteristic].innerText )) == null ? '' : __t) +
'</textarea>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="clearfix"></div>\n\t\t\t\t\t</div>\n\t\t\t\t';
 } else if (characteristics[characteristic].view_type === 'img') { ;
__p += '\n\t\t\t\t\t<div class="content__item__text__item">\n\t\t\t\t\t\t<div class="content__item__text__item__titel">\n\t\t\t\t\t\t\t<h4>' +
((__t = ( characteristics[characteristic].view_name.toUpperCase().replace(/_/g, " ") + " : " )) == null ? '' : __t) +
'</h4>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="content__item__text__item__text">\n\t\t\t\t\t\t\t';
 if (characteristics[characteristic].innerText) { ;
__p += '\n\t\t\t\t\t\t\t\t';
 for (var i=0; i<characteristics[characteristic].innerText.length; i++) { ;
__p += '\n\t\t\t\t\t\t\t\t\t<div style="margin-bottom:15px;" class="col-sm-24 img_container" data-imgId="' +
((__t = ( characteristics[characteristic].img_id[i] )) == null ? '' : __t) +
'">\n\t\t\t\t\t\t\t\t\t\t<p><img style="width:20%;float:left;margin-right:15px;"src="images/Projects/' +
((__t = ( characteristics[characteristic].innerText[i] )) == null ? '' : __t) +
'"></p>\n\t\t\t\t\t\t\t\t\t\t<i class="img_delete_controller glyphicon glyphicon-remove" data-imgId="' +
((__t = ( characteristics[characteristic].img_id[i] )) == null ? '' : __t) +
'"></i>\n\t\t\t\t\t\t\t\t\t\t';
 if (!characteristics[characteristic].super_state[i]) { ;
__p += ' \n\t\t\t\t\t\t\t\t\t\t<i class="img_main_controller glyphicon glyphicon-ok" data-imgId="' +
((__t = ( characteristics[characteristic].img_id[i] )) == null ? '' : __t) +
'"></i>\n\t\t\t\t\t\t\t\t\t\t';
 } ;
__p += '\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t';
 } ;
__p += '\n\t\t\t\t\t\t\t';
 } ;
__p += '\n\t\t\t\t\t\t\t<input ';
 if ( characteristics[characteristic].img_config.multiple ) { ;
__p += ' multiple="multiple" ';
 } ;
__p += ' type="file" name="uploadFile" data-dbFieldName="' +
((__t = ( characteristic )) == null ? '' : __t) +
'"/>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="clearfix"></div>\n\t\t\t\t\t</div>\n\t\t\t\t';
 } ;
__p += ' \n\t\t\t';
 } ;
__p += '\n\t\t</div>\n\t\t<div class="controller controller--content__item">\n\t\t\t<div class="controller__item controller__item--cancel">\n\t\t\t\t<i class="glyphicon glyphicon-chevron-left"></i>\n\t\t\t</div>\n\t\t\t<div class="controller__item controller__item--save">\n\t\t\t\t<i class="glyphicon glyphicon-ok"></i>\n\t\t\t</div>\n\t\t\t<div class="controller__item controller__item--delete">\n\t\t\t\t<i class="glyphicon glyphicon-remove"></i>\n\t\t\t</div>\n\t\t</div>\n\t</form>';

}
return __p
};

this["JST"]["assets/js/admin/templates/defaultGreeting.ejs"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="content__item content__item--greeting">\n\t<h1 class="content__item__titel">Hello, My Friend</h1>\n\t<div class="content__item__text">\n\t\t<p>This is a staring page in your administration panel.</p>\n\t\t<p>Enjoy it</p>\n\t</div>\n</div>';

}
return __p
};