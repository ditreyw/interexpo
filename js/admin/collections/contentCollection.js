'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Collections.ContentCollection = (function(_super) {
  __extends(ContentCollection, _super);

  function ContentCollection() {
    return ContentCollection.__super__.constructor.apply(this, arguments);
  }

  ContentCollection.prototype.model = TZ.Models.ContentModel;

  ContentCollection.prototype.url = function() {
    return this.name;
  };

  ContentCollection.prototype.initialize = function() {};

  ContentCollection.prototype.parse = function(response) {
    if (response.model) {
      return response.model;
    }
    return response;
  };

  return ContentCollection;

})(Backbone.Collection);

//# sourceMappingURL=contentCollection.js.map
