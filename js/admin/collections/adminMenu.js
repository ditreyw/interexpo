'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Collections.AdminMenu = (function(_super) {
  __extends(AdminMenu, _super);

  function AdminMenu() {
    return AdminMenu.__super__.constructor.apply(this, arguments);
  }

  AdminMenu.prototype.model = TZ.Models.AdminMenuItem;

  AdminMenu.prototype.url = '/adminMenu';

  AdminMenu.prototype.parse = function(response) {
    var item, _i, _len, _results;
    _results = [];
    for (_i = 0, _len = response.length; _i < _len; _i++) {
      item = response[_i];
      _results.push(this.add({
        name: item.name,
        fields_needed: item.fields_needed,
        type: item.type,
        fields_needed_json: item.fields_needed_json
      }));
    }
    return _results;
  };

  return AdminMenu;

})(Backbone.Collection);

//# sourceMappingURL=adminMenu.js.map
