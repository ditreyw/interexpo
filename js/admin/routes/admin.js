'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Routers.MainAdmin = (function(_super) {
  __extends(MainAdmin, _super);

  function MainAdmin() {
    return MainAdmin.__super__.constructor.apply(this, arguments);
  }

  MainAdmin.prototype.routes = {
    "": "adminLogin",
    "*content": "contentCreator"
  };

  MainAdmin.prototype.initialize = function() {
    return Backbone.history.start({
      root: '/adm'
    });
  };

  MainAdmin.prototype.adminLogin = function() {
    var default_greeting;
    return default_greeting = new TZ.Views.DefaultGreeting({
      el: $('#content-wr')
    });
  };

  MainAdmin.prototype.contentCreator = function(contentName) {
    return vent.trigger("content:show" + contentName, contentName);
  };

  return MainAdmin;

})(Backbone.Router);

//# sourceMappingURL=admin.js.map
