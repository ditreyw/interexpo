window.TZ = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  Helpers: {},
  init: function() {
    'use strict';
    window.vent = _.extend({}, Backbone.Events);
    TZ.menu = new TZ.Collections.AdminMenu;
    return TZ.menu.fetch().done(function() {
      return new TZ.Views.App({
        collection: TZ.menu
      });
    });
  }
};

$(function() {
  'use strict';
  return TZ.init();
});

//# sourceMappingURL=mainAdmin.js.map
