'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Models.AdminMenuItem = (function(_super) {
  __extends(AdminMenuItem, _super);

  function AdminMenuItem() {
    return AdminMenuItem.__super__.constructor.apply(this, arguments);
  }

  AdminMenuItem.prototype.defaults = {
    name: '',
    submenus: '',
    type: ''
  };

  return AdminMenuItem;

})(Backbone.Model);

//# sourceMappingURL=adminMenuItem.js.map
