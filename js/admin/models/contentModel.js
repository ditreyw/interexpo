'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Models.ContentModel = (function(_super) {
  __extends(ContentModel, _super);

  function ContentModel() {
    return ContentModel.__super__.constructor.apply(this, arguments);
  }

  ContentModel.prototype.initialize = function() {};

  ContentModel.prototype.defaults = {
    name: '',
    characteristics: {},
    in_edit_state: false
  };

  ContentModel.prototype.validate = function(attrs, options) {
    var characteristic;
    if (attrs.name.replace(/\s/g, '') === "") {
      return "name is required";
    }
    for (characteristic in attrs.characteristics) {
      if (attrs.characteristics[characteristic].required && attrs[characteristic].replace(/\s/g, '') === "") {
        return "'" + attrs.characteristics[characteristic].view_name + "' is required";
      }
    }
  };

  ContentModel.prototype.parse = function(response, options) {
    var characteristics, colum, image, images_arr, images_id_arr, images_super_state_arr, obj, require_char, super_state, text_field, texts_arr, _i, _len, _ref;
    if (response.error) {
      this.trigger('server_vaildation_error', this, response);
      return false;
    }
    texts_arr = [];
    if (_.isArray(response)) {
      response = response[0];
    }
    characteristics = $.extend(true, {}, this.collection.characteristics);
    for (text_field in characteristics) {
      characteristics[text_field].innerText = void 0;
      colum = void 0;
      require_char = void 0;
      super_state = void 0;
      if (characteristics[text_field].relation) {
        colum = characteristics[text_field].relation.colum_name;
        require_char = characteristics[text_field].relation.require;
      }
      if (characteristics[text_field].state) {
        super_state = characteristics[text_field].state["super"];
      }
      if (characteristics[text_field].view_type === 'img') {
        if (characteristics[text_field].relation) {
          images_arr = [];
          images_id_arr = [];
          images_super_state_arr = [];
          _ref = response[text_field];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            image = _ref[_i];
            if (require_char != null) {
              if (image[require_char]) {
                images_arr.push(image[colum]);
                images_id_arr.push(image.id);
              }
            } else {
              images_arr.push(image[colum]);
              images_id_arr.push(image.id);
            }
            if (super_state != null) {
              images_super_state_arr.push(image[super_state]);
            }
          }
          characteristics[text_field].innerText = images_arr;
          characteristics[text_field].img_id = images_id_arr;
          if (images_super_state_arr != null) {
            characteristics[text_field].super_state = images_super_state_arr;
          }
        } else {
          characteristics[text_field].innerText = response[text_field].replace(/\s/g, '').split(',');
        }
      } else {
        characteristics[text_field].innerText = response[text_field];
      }
    }
    console.log(characteristics);
    return obj = {
      id: response.id,
      name: response.name,
      characteristics: characteristics
    };
  };

  return ContentModel;

})(Backbone.Model);

//# sourceMappingURL=contentModel.js.map
