'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Views.ContentModelView = (function(_super) {
  __extends(ContentModelView, _super);

  function ContentModelView() {
    return ContentModelView.__super__.constructor.apply(this, arguments);
  }

  ContentModelView.prototype.template = JST['assets/js/admin/templates/contentModelView.ejs'];

  ContentModelView.prototype.tagName = 'div';

  ContentModelView.prototype.id = '';

  ContentModelView.prototype.className = 'content__item';

  ContentModelView.prototype.events = {
    'click .controller__item--expande': 'expande',
    'click .controller__item--collapse': 'collapse',
    'click .controller__item--edit': 'edit_state',
    'click .controller__item--cancel': 'cancle_edit_state',
    'click .controller__item--save': 'save',
    'click .controller__item--delete': 'delete',
    'keyup textarea': 'textAreaAdjust',
    'click .img_delete_controller': 'deleteImg',
    'click .img_main_controller': 'changeToMain'
  };

  ContentModelView.prototype.initialize = function() {
    this.listenTo(this.model, 'change', this.render);
    return this.listenTo(this.model, 'invalid', this.invalidVent);
  };

  ContentModelView.prototype.render = function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  };

  ContentModelView.prototype.edit_state = function() {
    var textAreaAdjust;
    this.$el.addClass('editing');
    textAreaAdjust = this.textAreaAdjust;
    return this.$el.find('textarea').each(function() {
      return textAreaAdjust(this);
    });
  };

  ContentModelView.prototype.cancle_edit_state = function() {
    return this.$el.removeClass('editing');
  };

  ContentModelView.prototype.invalidVent = function(model, error) {
    return alert(error);
  };

  ContentModelView.prototype.save = function() {
    var errorHandler, successHandler, values;
    successHandler = (function(_this) {
      return function(res) {
        return _this.$el.removeClass('editing');
      };
    })(this);
    errorHandler = (function(_this) {
      return function(model, error) {
        console.log(model);
        console.log(error);
        return alert(error.summary);
      };
    })(this);
    values = {};
    _.each(this.$('form').serializeArray(), function(input) {
      return values[input.name] = input.value;
    });
    this.model.on('server_vaildation_error', errorHandler);
    return this.model.save(values, {
      iframe: true,
      files: this.$('form :file'),
      data: values,
      success: successHandler,
      error: errorHandler,
      wait: true
    });
  };

  ContentModelView.prototype["delete"] = function() {
    if (confirm("Вы точно хотите удалить " + (this.model.get('name')) + "?")) {
      this.remove();
      return this.model.destroy();
    }
  };

  ContentModelView.prototype.textAreaAdjust = function(o) {
    return TZ.Helpers.TextAreaAdjust(o);
  };

  ContentModelView.prototype.expande = function() {
    return this.$el.addClass('expanded');
  };

  ContentModelView.prototype.collapse = function() {
    return this.$el.removeClass('expanded');
  };

  ContentModelView.prototype.deleteImg = function(e) {
    var delete_img_ellement, image_id;
    image_id = $(e.currentTarget).attr('data-imgId');
    delete_img_ellement = (function(_this) {
      return function() {
        return _this.$(".img_container[data-imgId='" + image_id + "']").remove();
      };
    })(this);
    return $.ajax({
      url: 'http://localhost:1337/projectimages/' + image_id,
      method: "DELETE"
    }).done((function(_this) {
      return function(msg) {
        return delete_img_ellement();
      };
    })(this));
  };

  ContentModelView.prototype.changeToMain = function(e) {
    var image_id;
    image_id = $(e.currentTarget).attr('data-imgId');
    return $.ajax({
      url: 'http://localhost:1337/projectimages/' + image_id,
      method: "POST",
      data: {
        "mainImg": true
      }
    }).done((function(_this) {
      return function(msg) {
        return _this.model.fetch();
      };
    })(this));
  };

  return ContentModelView;

})(Backbone.View);

//# sourceMappingURL=adminContentModelView.js.map
