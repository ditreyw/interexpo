'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Views.AdminMenu = (function(_super) {
  __extends(AdminMenu, _super);

  function AdminMenu() {
    return AdminMenu.__super__.constructor.apply(this, arguments);
  }

  AdminMenu.prototype.tagName = 'ul';

  AdminMenu.prototype.id = 'adminMenu';

  AdminMenu.prototype.className = '';

  AdminMenu.prototype.events = {};

  AdminMenu.prototype.initialize = function() {
    return this.listenTo(this.collection, 'change', this.render);
  };

  AdminMenu.prototype.render = function() {
    this.collection.each(this.addOne, this);
    return this;
  };

  AdminMenu.prototype.addOne = function(menuItem) {
    menuItem = new TZ.Views.AdminMenuItem({
      model: menuItem
    });
    return this.$el.append(menuItem.render().el);
  };

  return AdminMenu;

})(Backbone.View);

//# sourceMappingURL=adminMenu.js.map
