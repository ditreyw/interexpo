'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Views.AdminMenuItem = (function(_super) {
  __extends(AdminMenuItem, _super);

  function AdminMenuItem() {
    return AdminMenuItem.__super__.constructor.apply(this, arguments);
  }

  AdminMenuItem.prototype.template = JST['assets/js/admin/templates/adminMenuItem.ejs'];

  AdminMenuItem.prototype.tagName = 'li';

  AdminMenuItem.prototype.id = '';

  AdminMenuItem.prototype.className = 'adminMenu__item';

  AdminMenuItem.prototype.events = {
    'click': 'openContent'
  };

  AdminMenuItem.prototype.initialize = function() {
    var name;
    name = this.model.get('name');
    console.log(this.model);
    return vent.on("content:show" + name, this.createContent, this);
  };

  AdminMenuItem.prototype.render = function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  };

  AdminMenuItem.prototype.createContent = function(contentName) {
    var characteristics, content_collection, content_req_name, json_fields, name, texts_name_arr;
    console.log("Vent is working : " + contentName);
    this.setActiveMenuItem();
    json_fields = this.model.get('fields_needed_json');
    texts_name_arr = [];
    characteristics = json_fields;
    for (name in json_fields) {
      texts_name_arr.push(name);
    }
    content_req_name = this.model.get('name');
    content_collection = new TZ.Collections.ContentCollection;
    content_collection.name = content_req_name;
    content_collection.texts_name_arr = texts_name_arr;
    content_collection.characteristics = characteristics;
    $('#content-wr').removeClass('activated').addClass('deactivated');
    return setTimeout((function() {
      $('#content-wr').empty();
      return content_collection.fetch().done(function() {
        var contentView;
        contentView = new TZ.Views.ContentView({
          collection: content_collection,
          el: '#content-wr'
        });
        contentView.render();
        return $('#content-wr').removeClass('deactivated').addClass('activated');
      });
    }), 500);
  };

  AdminMenuItem.prototype.setActiveMenuItem = function(e) {
    $("." + this.$el[0].classList[0]).removeClass('active');
    return this.$el.addClass('active');
  };

  return AdminMenuItem;

})(Backbone.View);

//# sourceMappingURL=adminMenuItem.js.map
