'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Views.ContentPanelView = (function(_super) {
  __extends(ContentPanelView, _super);

  function ContentPanelView() {
    return ContentPanelView.__super__.constructor.apply(this, arguments);
  }

  ContentPanelView.prototype.template = JST['assets/js/admin/templates/adminContentPanel.ejs'];

  ContentPanelView.prototype.tagName = 'div';

  ContentPanelView.prototype.id = '';

  ContentPanelView.prototype.className = 'col-sm-24 content__panel';

  ContentPanelView.prototype.events = {
    'click .controller--content-panel__item--create': 'createItemForm',
    'click .controller--content-panel__item--refresh': 'refreshPage'
  };

  ContentPanelView.prototype.initialize = function() {};

  ContentPanelView.prototype.render = function() {
    var panelInfo;
    panelInfo = {};
    panelInfo.name = this.collection.name;
    this.$el.html(this.template(panelInfo));
    return this;
  };

  ContentPanelView.prototype.createItemForm = function() {
    var new_content_model, new_content_model_form, new_content_model_form_element;
    new_content_model = new TZ.Models.ContentModel({
      characteristics: this.collection.characteristics
    });
    new_content_model.urlRoot = '/' + this.collection.url();
    new_content_model_form = new TZ.Views.ContentModelView({
      model: new_content_model
    });
    new_content_model_form.edit_state();
    new_content_model_form.expande();
    new_content_model_form_element = new_content_model_form.render().$el;
    new_content_model_form.$('.controller__item--delete').remove();
    new_content_model_form.$('.controller__item--cancel').click((function(_this) {
      return function() {
        new_content_model_form.remove();
        return new_content_model.destroy();
      };
    })(this));
    new_content_model_form.$('.controller__item--save').click((function(_this) {
      return function() {
        return _this.collection.add(new_content_model);
      };
    })(this));
    new_content_model_form_element.addClass('content__item--content-panel');
    this.$('.content-panel__form-wr').prepend(new_content_model_form_element);
    return new_content_model.on('sync', (function(_this) {
      return function() {
        return new_content_model_form.remove();
      };
    })(this));
  };

  ContentPanelView.prototype.textAreaAdjust = function(o) {};

  ContentPanelView.prototype.refreshPage = function() {
    return this.collection.fetch({
      remove: false
    });
  };

  return ContentPanelView;

})(Backbone.View);

//# sourceMappingURL=adminContentPanelView.js.map
