'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Views.DefaultGreeting = (function(_super) {
  __extends(DefaultGreeting, _super);

  function DefaultGreeting() {
    return DefaultGreeting.__super__.constructor.apply(this, arguments);
  }

  DefaultGreeting.prototype.template = JST['assets/js/admin/templates/defaultGreeting.ejs'];

  DefaultGreeting.prototype.tagName = 'div';

  DefaultGreeting.prototype.id = 'defaultGreeting-wr';

  DefaultGreeting.prototype.className = '';

  DefaultGreeting.prototype.events = {};

  DefaultGreeting.prototype.initialize = function() {
    return this.render();
  };

  DefaultGreeting.prototype.render = function() {
    this.$el.html(this.template());
    return $('#content-wr').append(this.$el);
  };

  return DefaultGreeting;

})(Backbone.View);

//# sourceMappingURL=defaultGreeting.js.map
