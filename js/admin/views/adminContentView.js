'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Views.ContentView = (function(_super) {
  __extends(ContentView, _super);

  function ContentView() {
    return ContentView.__super__.constructor.apply(this, arguments);
  }

  ContentView.prototype.events = {};

  ContentView.prototype.initialize = function() {
    return this.listenTo(this.collection, 'add', this.addOne);
  };

  ContentView.prototype.render = function() {
    var contentPanel;
    contentPanel = new TZ.Views.ContentPanelView({
      collection: this.collection
    });
    this.$el.prepend(contentPanel.render().el);
    this.collection.each(this.addOne, this);
    return this;
  };

  ContentView.prototype.addOne = function(contentItem) {
    contentItem = new TZ.Views.ContentModelView({
      model: contentItem
    });
    return this.$el.append(contentItem.render().el);
  };

  return ContentView;

})(Backbone.View);

//# sourceMappingURL=adminContentView.js.map
