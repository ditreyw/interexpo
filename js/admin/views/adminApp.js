'use strict';
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TZ.Views.App = (function(_super) {
  __extends(App, _super);

  function App() {
    return App.__super__.constructor.apply(this, arguments);
  }

  App.prototype.events = {};

  App.prototype.initialize = function() {
    var menuView;
    TZ.Helpers.TextAreaAdjust = function(o) {
      var target;
      target = o.target || o;
      target.style.height = "1px";
      return target.style.height = (25 + target.scrollHeight) + "px";
    };
    menuView = new TZ.Views.AdminMenu({
      collection: this.collection
    });
    $('#nav').append(menuView.render().el);
    return TZ.router = new TZ.Routers.MainAdmin;
  };

  App.prototype.render = function() {};

  return App;

})(Backbone.View);

//# sourceMappingURL=adminApp.js.map
