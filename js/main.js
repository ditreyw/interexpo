'use strict';

/*
INITIAL
 */
var Menu, Portfolio_hover_window, Project_modal, addCSSRule, centerDivAbsolute, centerDivMargin, fadeIn_blocks_animation, mediator, mobile, port_width, scrollToAnchor, service_section_blocks, sheet, window_height, window_width;

sheet = document.styleSheets[0];

mediator = new Mediator;

mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && $(window).innerWidth() < 935;

addCSSRule = function(selector, rules) {
  if ('insertRule' in sheet) {
    return sheet.insertRule("" + selector + " { " + rules + " }", sheet.cssRules.length);
  } else if ('addRule' in sheet) {
    return sheet.addRule(selector, rules, sheet.cssRules.length);
  }
};

centerDivAbsolute = function(selector, horizontal) {
  var element, the_rules, _i, _len, _results;
  the_rules = function(selector) {
    var element_height, element_width;
    element_width = $(selector).width();
    element_height = $(selector).height();
    addCSSRule(selector, "height: " + element_height + "px; top: 0; bottom: 0; left: 0; right: 0; margin: auto; position: absolute;");
    if (horizontal) {
      return addCSSRule(selector, "width: " + element_width + "px;");
    }
  };
  if (typeof selector === "object") {
    _results = [];
    for (_i = 0, _len = selector.length; _i < _len; _i++) {
      element = selector[_i];
      _results.push(the_rules(element));
    }
    return _results;
  } else {
    return the_rules(selector);
  }
};

centerDivMargin = function(selector) {
  var element, the_rules, _i, _len, _results;
  the_rules = function(selector) {
    var element_height, element_width;
    element_width = $(selector).width();
    element_height = $(selector).height();
    return addCSSRule(selector, "height: " + element_height + "px; top: 50%; margin-top: -" + (element_height / 2) + "px; position: relative;");
  };
  if (typeof selector === "object") {
    _results = [];
    for (_i = 0, _len = selector.length; _i < _len; _i++) {
      element = selector[_i];
      _results.push(the_rules(element));
    }
    return _results;
  } else {
    return the_rules(selector);
  }
};

scrollToAnchor = function(aim, callback) {
  var aTag;
  aTag = $("#" + aim);
  return $('html,body').animate({
    scrollTop: aTag.offset().top + 5
  }, 'slow', function() {
    if (callback != null) {
      return callback();
    }
  });
};

window_height = $(window).innerHeight();

window_width = $(window).innerWidth();

port_width = $('.portfolio-section-project').width();

$('.portfolio-section-project').height(port_width);

$(window).load(function() {
  var $body, an, animation, menu, portfolio_hover_window, sertificate_sec_height, service_block, set_DOM_height, timer, video_pos_x, video_pos_y;
  if (!mobile) {
    $('#certificate-link').unbind('click').click(function(e) {
      var img, modal;
      e.preventDefault();
      img = $('<img>', {
        id: 'certificate-modal-window-image',
        src: '../images/about2.jpg'
      });
      modal = $('<div/>', {
        id: 'certificate-modal-window',
        "class": 'deactive'
      });
      modal.append(img);
      $('body').append(modal);
      modal.click(function() {
        modal.removeClass('active').addClass('deactive');
        return setTimeout((function() {
          return modal.remove();
        }), 300);
      });
      return setTimeout((function() {
        return modal.removeClass('deactive').addClass('active');
      }), 100);
    });
    animation = new fadeIn_blocks_animation('.animation-sec');
    animation.init();
    $('.clients-slider-controller').unbind('click').click(function() {
      return $('.clients-square-slider').toggleClass('next-list');
    });
    portfolio_hover_window = new Portfolio_hover_window('.hover-portfolio-section-project');
    portfolio_hover_window.hover_effect();
    service_block = new service_section_blocks($('.service-section-block'));
    service_block.set_active_background();
    service_block.set_background_height();
    sertificate_sec_height = $('.sertificate-section').height();
    $('.sertificate-section > div').height(sertificate_sec_height * 0.7);
    timer = "";
    $body = $('body');
  }
  menu = new Menu($("nav"), $(".menu-list-item-link"), 'active-link');
  menu.set_scroll_animation();
  $.ajax({
    url: 'http://localhost:1337/projectimages',
    method: 'GET'
  }).done(function(data) {
    var image_obj, images, project_modal, project_name, _i, _len;
    images = {};
    for (_i = 0, _len = data.length; _i < _len; _i++) {
      image_obj = data[_i];
      project_name = image_obj.project_id.name.toLowerCase().replace(" ", "");
      if (!images[project_name]) {
        images[project_name] = [];
        images[project_name].push(image_obj.image);
      } else {
        images[project_name].push(image_obj.image);
      }
    }
    project_modal = new Project_modal('.inner-portfolio-section-project', '.close-project', images);
    return project_modal.init();
  });
  set_DOM_height = function() {
    var gone_throw, target, _results;
    target = $('.main-WR');
    gone_throw = [];
    while (target.children().length > 0) {
      target = target.children();
      gone_throw.push(target);
    }
    _results = [];
    while (gone_throw.length > -1 && (target != null)) {
      $.each(target, function() {
        var h;
        if ($(this)[0].clientHeight >= $(this)[0].scrollHeight && $(this).css('display') !== 'none') {
          h = $(this)[0].clientHeight;
        } else if ($(this)[0].clientHeight < $(this)[0].scrollHeight && $(this).css('display') !== 'none') {
          h = $(this)[0].scrollHeight;
        }
        return $(this).height(h);
      });
      target = gone_throw[gone_throw.length - 1];
      _results.push(gone_throw.splice(gone_throw.length - 1));
    }
    return _results;
  };
  if (mobile) {
    set_DOM_height();
    $('.service-section-block').height(window_height / 2);
    $('.logo-WR, .menu-list, .menu-controllers').addClass('deactive');
    menu.open_mobile_menu();
  }
  if ($('.main-WR').width() / window.innerHeight < 2) {
    $('video').css('height', '100%');
  } else {
    $('video').css('width', '100%');
  }
  video_pos_x = ($('video').width() - $('.main-WR').width()) / -2;
  video_pos_y = ($('video').height() - $('.main-WR').height()) / -2;
  $('video').css({
    'top': video_pos_y,
    'left': video_pos_x
  });
  $('.service-section-block .overlay').height('100%');
  if (!mobile) {
    menu.set_active_on_scroll();
  }
  setTimeout((function() {
    return $(window).scrollTop(0);
  }), 0);
  an = false;
  $.fn.mousewheel = function(handler) {
    var target;
    target = $(this)[0];
    if (target.addEventListener) {
      target.addEventListener("mousewheel", handler, false);
      target.addEventListener("DOMMouseScroll", handler, false);
    } else {
      target.attachEvent("onmousewheel", handler);
    }
    return this;
  };
  return $('body').mousewheel(function(e) {
    var delta;
    if (window.pageYOffset < window_height - 20) {
      e.preventDefault();
    }
    e = window.event || e;
    delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail || -e.originalEvent.detail));
    if (window.pageYOffset < window_height - 20 && delta > 0 && !an) {
      an = true;
      scrollToAnchor('first-section-float-emulation', function() {
        return an = false;
      });
    }
    if (window.pageYOffset < window_height - 20 && delta < 0 && !an) {
      an = true;
      return scrollToAnchor('about-company-section', function() {
        return an = false;
      });
    }
  });
});

Menu = (function() {
  function Menu($menu_element, $nav_link, active_menu_link_class) {
    this.$menu_element = $menu_element;
    this.$nav_link = $nav_link;
    this.active_menu_link_class = active_menu_link_class;
    this.active_link = false;
    this.counter = 0;
  }

  Menu.prototype.set_scroll_animation = function() {
    var self;
    self = this;
    return this.$nav_link.unbind('click').click(function(event) {
      event.preventDefault();
      scrollToAnchor("" + ($(this).attr('name')));
      return $(self).trigger('menu-clicked');
    });
  };

  Menu.prototype.set_active_on_scroll = function() {
    var link_elements, name, section_elements_offsets, section_names, self, _i, _len;
    self = this;
    section_names = [];
    $('.menu-list-item-link').each(function() {
      return section_names.push($(this).attr('name'));
    });
    section_elements_offsets = [];
    link_elements = self.$nav_link.toArray();
    for (_i = 0, _len = section_names.length; _i < _len; _i++) {
      name = section_names[_i];
      section_elements_offsets.push($(".main-WR #" + name).offset().top);
    }
    return $(window).scroll(function(event) {
      if (window.pageYOffset > section_elements_offsets[self.counter]) {
        if (self.counter > 0) {
          $(link_elements[self.counter - 1]).removeClass('active-link');
        }
        $(link_elements[self.counter]).addClass('active-link');
        return self.counter++;
      } else if (window.pageYOffset < section_elements_offsets[self.counter - 1]) {
        $(link_elements[self.counter - 1]).removeClass('active-link');
        $(link_elements[self.counter - 2]).addClass('active-link');
        return self.counter--;
      }
    });
  };

  Menu.prototype.open_mobile_menu = function() {
    var self;
    self = this;
    return $('.mobile-menu').unbind('click').on('click', function() {
      event.preventDefault();
      $('.logo-WR, .menu-list, .menu-controllers').removeClass('deactive');
      self.$menu_element.addClass('activated-menu');
      $(this).unbind('click').click(function() {
        return self.close_mobile_menu();
      });
      return self.$nav_link.unbind('click').click(function(event) {
        event.preventDefault();
        scrollToAnchor("" + ($(this).attr('name')));
        $(self).trigger('menu-clicked');
        return self.close_mobile_menu();
      });
    });
  };

  Menu.prototype.close_mobile_menu = function() {
    var self;
    self = this;
    $('.logo-WR, .menu-list, .menu-controllers').addClass('deactive');
    self.$menu_element.removeClass('activated-menu');
    return self.open_mobile_menu();
  };

  return Menu;

})();

service_section_blocks = (function() {
  function service_section_blocks($element) {
    this.$element = $element;
    this.element_height = this.$element.height();
    this.element_width = this.$element.width();
  }

  service_section_blocks.prototype.set_height = function(height) {
    return this.$element.height(height);
  };

  service_section_blocks.prototype.set_background_height = function() {
    var self, wrHeight;
    self = this;
    this.wr_width = $('.service-section-blocks-WR').width();
    wrHeight = window_height;
    this.$element.find('.service-section-block-bg').css('background-size', "" + this.wr_width + "px " + wrHeight + "px");
    return this.$element.hover(function() {
      return $(this).addClass("service-section-hovered");
    }, function() {
      return $(this).removeClass("service-section-hovered");
    });
  };

  service_section_blocks.prototype.set_active_background = function() {
    var self;
    self = this;
    return this.$element.unbind('click').click(function() {
      var $cliked;
      $cliked = $(this);
      self.$element.fadeOut(function() {
        $cliked.width(self.wr_width);
        $cliked.height(window_height);
        return $cliked.addClass('activated').fadeIn((function() {
          return $cliked.unbind('click').click(function() {
            return $cliked.fadeOut(function() {
              $cliked.removeClass('activated').width('50%').height(self.element_height);
              self.$element.fadeIn();
              return self.set_active_background();
            });
          });
        }));
      });
      return true;
    });
  };

  return service_section_blocks;

})();

Portfolio_hover_window = (function() {
  function Portfolio_hover_window(element) {
    this.element = element;
    this.$element_parent = $(this.element).parent();
    this.modal_height = $(this.element).height();
    this.modal_width = $(this.element).width();
  }

  Portfolio_hover_window.prototype.hover_effect = function() {
    var get_dir, get_style, mousemove, self;
    self = this;
    mousemove = function(event) {
      var $el, direction, style;
      direction = get_dir(this, {
        x: event.pageX,
        y: event.pageY
      });
      style = get_style(direction);
      $el = $(self.element, this);
      if (event.type === 'mouseenter') {
        $el.hide().css(style.from);
        return $el.show(0, function() {
          return $(this).stop().animate(style.to, 300);
        });
      } else {
        return $el.stop().animate(style.from, 300);
      }
    };
    get_dir = function(el, coordinates) {
      var direction, h, w, x, y;
      w = self.$element_parent.width();
      h = self.$element_parent.height();
      x = (coordinates.x - $(el).offset().left - (w / 2)) * (w > h ? h / w : 1);
      y = (coordinates.y - $(el).offset().top - (h / 2)) * (h > w ? w / h : 1);
      return direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;
    };
    get_style = function(direction) {
      fromStyle;
      toStyle;
      var fromStyle, slideFromBottom, slideFromLeft, slideFromRight, slideFromTop, slideLeft, slideTop, toStyle;
      slideFromTop = {
        left: '0px',
        top: '-200%'
      };
      slideFromBottom = {
        left: '0px',
        top: '200%'
      };
      slideFromLeft = {
        left: '-200%',
        top: '0px'
      };
      slideFromRight = {
        left: '200%',
        top: '0px'
      };
      slideTop = {
        top: '0px'
      };
      slideLeft = {
        left: '0px'
      };
      switch (direction) {
        case 0:
          fromStyle = slideFromTop;
          toStyle = slideTop;
          break;
        case 1:
          fromStyle = slideFromRight;
          toStyle = slideLeft;
          break;
        case 2:
          fromStyle = slideFromBottom;
          toStyle = slideTop;
          break;
        case 3:
          fromStyle = slideFromLeft;
          toStyle = slideLeft;
      }
      return {
        from: fromStyle,
        to: toStyle
      };
    };
    return this.$element_parent.on('mouseenter mouseleave', mousemove);
  };

  return Portfolio_hover_window;

})();

Project_modal = (function() {
  function Project_modal(element, close_btn, project_modal_images) {
    var index, proj, project_elements, _i, _len;
    this.element = element;
    this.close_btn = close_btn;
    this.project_modal_images = project_modal_images;
    this.names_obj = {};
    project_elements = $('.inner-portfolio-section-project');
    for (index = _i = 0, _len = project_elements.length; _i < _len; index = ++_i) {
      proj = project_elements[index];
      this.names_obj[$(proj).data('project')] = $(proj).data('project-img-number');
    }
    this.activated_project_name = '';
    this.activated_project_text = '';
    this.$active_project_image;
    this.project_queu = [];
  }

  Project_modal.prototype.init = function() {
    var number, project, self, _ref;
    self = this;
    _ref = this.names_obj;
    for (project in _ref) {
      number = _ref[project];
      self.project_queu.push(project);
    }
    return this.open_modal();
  };

  Project_modal.prototype.open_modal = function() {
    var self;
    self = this;
    $(this.element).unbind('click').click(function() {
      self.activated_project_name = $(this).data().project;
      self.activated_project_text = '.modal-window-text#' + self.activated_project_name;
      $('.modal-window').addClass('active');
      setTimeout((function() {
        self._set_project_change_queu();
        self.create_images();
        return $(self.activated_project_text).removeClass('deactive').addClass('active');
      }), 320);
      return $('html').css('overflow', 'hidden');
    });
    this._close_modal();
    this._change_project();
    return true;
  };

  Project_modal.prototype._close_modal = function() {
    var self;
    self = this;
    $(this.close_btn).unbind('click').click(function() {
      $(self.activated_project_text).removeClass('active').addClass('deactive');
      $('.modal-window').removeClass('active');
      $('html').css('overflow', 'initial');
      return $('.modal-window-image-wr').empty();
    });
    return true;
  };

  Project_modal.prototype.set_next_image = function(element) {
    return element.removeClass('deactive').addClass('active');
  };

  Project_modal.prototype.set_prev_image = function(element) {
    return element.removeClass('active').addClass('deactive');
  };

  Project_modal.prototype.create_images = function() {
    var image, images_length, modal_window_image, modal_window_image_class, modal_window_image_wr, modal_window_image_wr_class, self, _i, _len, _ref;
    self = this;
    images_length = this.project_modal_images[self.activated_project_name].length;
    _ref = this.project_modal_images[self.activated_project_name];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      image = _ref[_i];
      modal_window_image_wr_class = 'inner-modal-window-image-wr active';
      modal_window_image_class = 'modal-window-image';
      modal_window_image_wr = $('<div/>', {
        "class": modal_window_image_wr_class
      });
      modal_window_image = $('<img/>', {
        "class": modal_window_image_class,
        src: "../images/Projects/" + image
      });
      modal_window_image_wr.append(modal_window_image);
      $('.modal-window-image-wr').append(modal_window_image_wr);
    }
    return true;
  };

  Project_modal.prototype._set_project_change_queu = function() {
    var index, self;
    self = this;
    index = this.project_queu.indexOf(this.activated_project_name);
    if (index === 0) {
      $('.inner-modal-window-text-controller.prev-project').data('project', self.project_queu[self.project_queu.length - 1]);
      return $('.inner-modal-window-text-controller.next-project').data('project', self.project_queu[1]);
    } else if (index === this.project_queu.length - 1) {
      $('.inner-modal-window-text-controller.prev-project').data('project', self.project_queu[index - 1]);
      return $('.inner-modal-window-text-controller.next-project').data('project', self.project_queu[0]);
    } else {
      $('.inner-modal-window-text-controller.prev-project').data('project', self.project_queu[index - 1]);
      return $('.inner-modal-window-text-controller.next-project').data('project', self.project_queu[index + 1]);
    }
  };

  Project_modal.prototype._change_project = function() {
    var self;
    self = this;
    return $('.inner-modal-window-text-controller.prev-project, .inner-modal-window-text-controller.next-project').unbind('click').click(function() {
      $(self.activated_project_text).removeClass('active').addClass('deactive');
      self.activated_project_name = $(this).data().project;
      self.activated_project_text = '.modal-window-text#' + self.activated_project_name;
      $(self.activated_project_text).removeClass('deactive').addClass('active');
      $('.modal-window-image-wr').empty();
      self.create_images();
      return self._set_project_change_queu();
    });
  };

  return Project_modal;

})();

fadeIn_blocks_animation = (function() {
  function fadeIn_blocks_animation(el) {
    this.el = el;
    this.$el = $(this.el);
    this.el_array = [];
    this.el_y_pos = [];
  }

  fadeIn_blocks_animation.prototype.init = function() {
    var self;
    self = this;
    $.each(this.$el, function() {
      return self.el_y_pos.push($(this).offset().top - window_height * 0.50);
    });
    $(this.$el).addClass('transition before-animated');
    return this.onscroll_animation();
  };

  fadeIn_blocks_animation.prototype.onscroll_animation = function() {
    var fadeIn_animation, self;
    self = this;
    this.next_offset_counter = 0;
    this.prev_offset_counter = -1;
    fadeIn_animation = function() {
      self.next_section_offset = self.el_y_pos[self.next_offset_counter];
      self.prev_section_offset = self.el_y_pos[self.prev_offset_counter];
      if (window.pageYOffset > self.next_section_offset) {
        $(self.$el[self.next_offset_counter]).removeClass('before-animated').addClass('animated');
        self.next_offset_counter++;
        return self.prev_offset_counter++;
      } else if (window.pageYOffset < self.prev_section_offset) {
        self.next_offset_counter--;
        return self.prev_offset_counter--;
      }
    };
    fadeIn_animation();
    return $(window).scroll(function(event) {
      return fadeIn_animation();
    });
  };

  return fadeIn_blocks_animation;

})();

//# sourceMappingURL=main.js.map
